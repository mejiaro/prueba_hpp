class ChangeDefaultValueEstadoToCodigos < ActiveRecord::Migration
  def change
    change_column :codigos, :estado, :string, :default => 'Inactivo'
  end
end
