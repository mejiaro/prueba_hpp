class AddUserIdToCodigos < ActiveRecord::Migration
  def change
    add_column :codigos, :user_id, :integer
    
    add_foreign_key :codigos, :users
    add_index :codigos, [:user_id, :created_at]
  end
end
