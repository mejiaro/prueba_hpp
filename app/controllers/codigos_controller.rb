class CodigosController < ApplicationController
  before_action :set_codigo, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:home]
  
  def new
    @codigo = Codigo.new
  end
  
  def create
    
    @codigo = Codigo.new(codigo_params)
    if @codigo.save
      redirect_to @codigo, notice: 'Código creado.'
    else
      render 'new'
    end
  end

  def index
    if !current_user.blank?
      @codigos = current_user.codigos
    else
      @codigos = Codigo.all
    end
  end

  def show
    
  end

  def destroy
    
    @codigo.destroy
     redirect_to codigos_url, notice: 'Codigo eliminado.'
  end

  def edit
  end
  
  def update
    if @codigo.update(codigo_params)
        redirect_to @codigo, notice: 'Codigo editado.'
    else
      render 'edit'
    end
  end
  
  def home
    @codigos = current_user.codigos
  end
  
  def activate
    codigo = Codigo.find_by(descripcion: params[:descripcion])
    if codigo.blank?
      redirect_to codigos_home_url, notice: 'Código Inválido'
    else
      codigo.update_attributes(user_id: current_user.id, estado: 'Activo')
      redirect_to root_url, notice: 'Código activado.'
    end
  end
  
  def activar
    
  end

 
  
  private
    def codigo_params
      params.require(:codigo).permit(:descripcion, :estado)
    end
    
    def set_codigo
      @codigo = Codigo.find_by(id: params[:id])
    end
end
