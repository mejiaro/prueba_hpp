class Codigo < ActiveRecord::Base
  belongs_to :user
  
  VALID_CODE = /([A-Z0-9])/
  validates :descripcion, format: {with: VALID_CODE}, presence: true, length: { maximum: 8, minimum:8},
            uniqueness: true
end
